# masterthesisiot

This repository contains the dataset and a machine learning program created for our master thesis and which we decided to share.

Feel free to use the dataset we created.

## Content

- The input_data folder contains the diferent datasets we created in .csv format.
- The ML.py file contains the main machine learning program.
The other python files contains helper functions used by the program.
    - build_models : list the machine learning models considerd
    - scorings : list the different scoring functions considered
    - settings : shows the settings used by the main porgram

## The Dataset

The dataset is composed of two files :
- the environmental datas (CO2, humidity, temperature, light and motion)
- the supervision data (the number of people in the room)

We colleced those data in two different rooms (more information in our master thesis).

The goal was to predict the number of peaple from the environmental data.


## The Program

The pyton program test several machine learning techniques and outputs their performence, learning curve, feature importance and other metrics.

## Our Master Thesis

If you want additional information about the dataset or the program, it is detailed in our master thesis (link to be added soon).

### Authors

Nicolas Bockstael and Alexandre Jadin